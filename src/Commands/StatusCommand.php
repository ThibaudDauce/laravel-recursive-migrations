<?php declare(strict_types = 1);

namespace ThibaudDauce\LaravelRecursiveMigrations\Commands;

use ThibaudDauce\LaravelRecursiveMigrations\RecursiveMigrationCommand;
use Illuminate\Database\Console\Migrations\StatusCommand as BaseStatusCommand;

class StatusCommand extends BaseStatusCommand
{
    use RecursiveMigrationCommand;
}