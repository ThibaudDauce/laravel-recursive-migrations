<?php declare(strict_types = 1);

namespace ThibaudDauce\LaravelRecursiveMigrations\Commands;

use ThibaudDauce\LaravelRecursiveMigrations\RecursiveMigrationCommand;
use Illuminate\Database\Console\Migrations\ResetCommand as BaseResetCommand;

class ResetCommand extends BaseResetCommand
{
    use RecursiveMigrationCommand;
}