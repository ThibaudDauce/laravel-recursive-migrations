<?php declare(strict_types = 1);

namespace ThibaudDauce\LaravelRecursiveMigrations\Commands;

use ThibaudDauce\LaravelRecursiveMigrations\RecursiveMigrationCommand;
use Illuminate\Database\Console\Migrations\RefreshCommand as BaseRefreshCommand;

class RefreshCommand extends BaseRefreshCommand
{
    use RecursiveMigrationCommand;
}