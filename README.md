# Laravel Recursive Migration

This package allows to put Laravel migrations into subdirectories.

Check [my blog post](https://thibaud.dauce.fr/posts/2016-12-15-laravel-recursive-migrations.html)!

## Installation

Download the package with Composer:

```bash
composer require thibaud-dauce/laravel-recursive-migration
```

Then add the Service Provider in you `config/app.php`:

```
ThibaudDauce\LaravelRecursiveMigrations\LaravelRecursiveMigrationsServiceProvider::class,
```

## Usage

All migrations' commands are available. Just add the `--recursive` or the `-r` flag.

```bash
php artisan migrate --recursive
php artisan migrate:rollback --recursive
php artisan migrate:refresh --recursive
php artisan migrate:reset --recursive
php artisan migrate:status --recursive
```
